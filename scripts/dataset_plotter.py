#!venv/bin/python
# encoding: utf-8

import json
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


############################
# utils
############################

ONE_MB = 1024**2
ONE_GB = 1024**3

def get_rel_path(path):
    import functools
    if not os.path.exists(path):
        os.makedirs(path)
    return functools.partial(os.path.join, path)

############################
# Plotting/Figures
############################


def save_close(fig, output_image_path, **kwargs):
    if fig is None: return
    dpi = kwargs.get("dpi", 200)
    format = kwargs.get("format", "pdf")
    fontsize = kwargs.get("fontsize", 20)
    ticks_fontsize = kwargs.get("ticks_fontsize", fontsize)
    def format_ax(ax):
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(ticks_fontsize)
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label]):
            item.set_fontsize(fontsize)
    for ax in fig.get_axes():
        format_ax(ax)
    fig.tight_layout()
    plt.tight_layout(pad=1.0, w_pad=0.5, h_pad=5.0)
    fig.savefig(output_image_path + '.' + format, format=format, dpi=dpi)
    plt.close(fig)


def hatch_cdfs(ax):
    lines = ax.patches
    # linestyle or ls: ['solid' , 'dashed', 'dashdot', 'dotted']
    linestyle = ['solid' , 'dashed', 'dashdot', 'dotted'] * 2
    linewidth = (2.0, ) * len(lines)
    for line, ls, lw in zip(lines, linestyle, linewidth):
        line.set_lw(lw)
        line.set_ls(ls)


def plot_images_size_distr(df, **kwargs):
    bins=500
    bins=10**(np.arange(1,10))
    bins=2**(np.arange(10,30))
    ax = df.groupby('image_id')['layer_size'].sum().plot.hist(
            cumulative=True, density=1, bins=bins, histtype='step')
    ax.set_xlabel('Image size')
    ax.set_ylabel('')
    ax.set_title('')
    ax.set_xscale('log')
    hatch_cdfs(ax)
    # ax.xaxis.set_major_locator(plt.LogLocator())
    # ax.xaxis.set_major_locator(plt.FixedLocator(10**(np.arange(3,10))))
    ax.set_xlim(1000, ax.get_xlim()[1])
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_layers_size_distr(df, **kwargs):
    bins=50
    bins=10**(np.arange(1,10))
    bins=2**(np.arange(1,30))
    ax = df['layer_size'].plot.hist(
            cumulative=True, density=1, bins=bins, histtype='step')
    ax.set_xlabel('Layer size')
    ax.set_ylabel('')
    ax.set_title('')
    ax.set_xscale('log')
    # ax.xaxis.set_major_locator(plt.LogLocator())
    ax.xaxis.set_major_locator(plt.FixedLocator(10**(np.arange(1,10))))
    hatch_cdfs(ax)
    ax.set_xlim(1, ax.get_xlim()[1])
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_layer_per_image_cdf(df, **kwargs):
    ax = df.groupby('image_id')['layer_id'].count().plot.hist(
            cumulative=True, density=1, bins=50, histtype='step')
    ax.set_xlabel('Layer per image')
    ax.set_ylabel('')
    ax.set_title('')
    hatch_cdfs(ax)
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()



def dataset_info(df_layers, df_images, df_images_full):
    print('Total #images', len(df_images.image_id.unique()))
    print('Total size of images %.2f GB' % (df_images_full.layer_size.sum() / ONE_GB))
    print('Total #layers', len(df_images))
    print('Total #unique layers', len(df_layers))
    print('Total size of unique layers %.2f GB' % (df_layers.layer_size.sum() / ONE_GB))



def main(input_dir, output_dir):

    awd = get_rel_path(output_dir)
    rwd = get_rel_path(input_dir)

    df_layers = pd.read_csv(rwd('layers.csv'))
    df_layers['layer_size'] = df_layers['layer_size'].apply(int)
    # df_layers['layer_size'] = df_layers['layer_size'] // ONE_GB

    images = json.load(open(rwd('images.json')))
    # df_req = pd.read_csv(rwd('image_requests.csv'))

    images_ids, layers_ids = [], []
    image_ids = sorted(images.keys())
    for image_id, image_layers in images.items():
        images_ids.extend([image_id] * len(image_layers))
        layers_ids.extend(image_layers)
    df_images = pd.DataFrame.from_dict({'image_id': images_ids, 'layer_id': layers_ids})

    df_images_full = pd.merge(df_images, df_layers, on='layer_id', how='left')

    # perc = [.8, .9, .95, .99]
    #
    # big = df_images_full[df_images_full['layer_size'] > 50 * 1024**2]
    # # vv = df_images_full.groupby('layer_id')['image_id'].count()
    # vv = big.groupby('image_id')['layer_id'].count()
    # print('number of big layers per image')
    # print(vv.describe(percentiles = perc))
    # vv = big.groupby('layer_id')['image_id'].count()
    # print('number of images per big layer')
    # # percentile list
    # print(vv.describe(percentiles = perc))
    # # print(vv.quantile(0.9))
    # return
    # # dataset_info(df_layers, df_images, df_images_full)

    save_close(
        plot_layers_size_distr(df_layers),
        awd("layers-size-cdf"))

    save_close(
        plot_layer_per_image_cdf(df_images),
        awd("layer-per-image-cdf"))

    save_close(
        plot_images_size_distr(df_images_full),
        awd("images-size-cdf"))



if __name__ == "__main__":

    if len(sys.argv) != 3:
        print('usage: python dataset_plotter.py input-dir output-dir')
        sys.exit(1)

    input_dir, output_dir = sys.argv[1:]

    main(input_dir, output_dir)
