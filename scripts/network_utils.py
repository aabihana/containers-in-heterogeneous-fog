
import json

INF_BW = 1024**4

def write_network_to_file(networks, output_path):
    json.dump(networks, open(output_path, 'w'))

def load_network_from_file(fname):
    return json.load(open(fname))
