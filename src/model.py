"""The Model module contains basic data models.
"""

from collections import namedtuple


Layer = namedtuple('Layer', ['id', 'nrep', 'size'])


class Infrastructure():
    """The Infrastructure object"""
    def __init__(self, name, nb_nodes, network, node_cap):
        self.name = name
        self.nb_nodes = nb_nodes
        self.network = network
        self.capacities = [node_cap] * nb_nodes
    def nodes(self):
        return range(self.nb_nodes)
