"""The simulation module that output a detailed results.

    This module output the retrieval time per image per node.
"""


import itertools

from data_loader import load_layers, load_images, load_network_from_file
from dataframe import BigDataframe
from model import Layer, Infrastructure
from placement import *
from retrieval_engine import RetrievalEngine


def run_expr(networks_file_id, retrieval_level, output_dir, config):
    """The main function of this module"""

    if retrieval_level != 'image':
        print("Just image level retrieval is supported with per_image_node output format.")
        return

    pid_str = 'run-%s-%s' % (networks_file_id, retrieval_level)
    print('start [%s] ..' % pid_str)

    csv_path = '%s/%s-level-%s.csv' % (output_dir, retrieval_level, networks_file_id)

    deterministic_placement_strategies = set([KcenterStrategy, KcenterWCStrategy])

    df = BigDataframe(csv_path,
                      ('iter', 'network_id', 'nb_nodes', 'repf', 'cap_scaling',
                       'placement', 'node', 'imageid', 'rtime', 't_percent'))

    def persist_results():
        df.flush()

    def create_big_layer_t_kwargs(layers, big_layer_threshold_percent):
        inv_bltp = 1 - big_layer_threshold_percent
        sizes = sorted([l.size for l in layers])[int(inv_bltp * len(layers)):]
        return {'big_layer_threshold': min(sizes)}

    ######################################################

    dataset_dir = config['DATASET_DIR']
    networks_dir = config['NETWORKS_DIR']

    layers = load_layers(dataset_dir)
    # layers = [l for l in layers if l.size > 10*1024**2]
    # layers = [sorted(layers)[-1]]
    # for l in layers: l.size = 100 * 1024**2

    networks = load_network_from_file(networks_dir, networks_file_id)

    images = load_images(dataset_dir)

    # just to reduce the runtime of the simulation
    # as small images have no impact on the maximum retrieval time
    big_images = dict()
    mp_layer_id = {l.id: l for l in layers}
    for imageid, im_layers_ids in images.iteritems():
        im_layers = [mp_layer_id[layer_id]
                     for layer_id in im_layers_ids]
        im_size = sum([l.size for l in im_layers])
        if im_size >= 100 * 1024**2:
            big_images[imageid] = im_layers_ids

    images = big_images
    big_images_layers = set(reduce(lambda x, y: x+y, images.values(), list()))
    layers = [l for l in layers if l.id in big_images_layers]

    ######################################################

    iterations = config['iterations']
    replication_factor_list = config['replication_factor_list']
    capacity_scaling_list = config['capacity_scaling_list']
    f_factor_list = config['f_factor_list']

    placement_strategies = [eval(p) for p in config['placement_strategies_list']]

    for expr_run in itertools.product(range(iterations),
                                      replication_factor_list,
                                      capacity_scaling_list,
                                      f_factor_list,
                                      list(networks.keys()),
                                      placement_strategies):

        itri, repf, cap_scaling, bltp, network_id, placement_class = expr_run

        # no more than one iteration for deterministic strategies
        if itri and placement_class in deterministic_placement_strategies:
            continue

        if placement_class != KcenterWCStrategy and bltp != 0.10:
            continue

        layers_rep = [Layer(l.id, repf, l.size) for l in layers]
        agg_ds_size = sum([l.size * l.nrep for l in layers_rep])

        bw_graph = networks[network_id]
        nb_nodes = len(bw_graph)

        if cap_scaling == -1:
            cap_scaling = nb_nodes

        node_cap = int(cap_scaling * agg_ds_size // nb_nodes)

        kwargs = create_big_layer_t_kwargs(layers, bltp)

        infra = Infrastructure(network_id, nb_nodes, bw_graph, node_cap)
        placement_strategy = placement_class(images, layers_rep, infra, **kwargs)

        print(placement_class.__name__, network_id, repf, cap_scaling)

        is_valid, placement = placement_strategy.place()

        if not is_valid:
            print("placement failed:", placement_class.__name__,
                  nb_nodes, network_id, repf, cap_scaling)
            df.add_row(itri, network_id, nb_nodes, repf, cap_scaling,
                       placement_class.__name__, -1, -1, -1, bltp)
            continue

        # continue
        # if not placement.verify():
        #     print('ERROR in layers placement !!')

        re = RetrievalEngine(placement, infra)

        if retrieval_level == 'image':
            mp_layer_id = {l.id: l for l in layers_rep}
            for node in range(nb_nodes):
                for imageid, im_layers_ids in images.iteritems():
                    im_layers = [mp_layer_id[layer_id]
                                 for layer_id in im_layers_ids]
                    im_size = sum([l.size for l in im_layers])
                    _, irtime = re.retrieve_layers_to_node(im_layers, node)
                    irtime = round(irtime, 4)

                    df.add_row(itri, network_id, nb_nodes, repf, cap_scaling,
                               placement_class.__name__, node, imageid, irtime, bltp)
        else:
            assert False

        # partial results in case of failure or long running simulation
        persist_results()


    persist_results()

    print('[%s] is done.' % pid_str)
